package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type slackOAuthResponse struct {
	Ok         bool   `json:"ok"`
	AppID      string `json:"app_id"`
	AuthedUser struct {
		ID          string `json:"id"`
		Scope       string `json:"scope"`
		AccessToken string `json:"access_token"`
		TokenType   string `json:"token_type"`
	} `json:"authed_user"`
	Team struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"team"`
	Enterprise interface{} `json:"enterprise"`
}

var slackResponse slackOAuthResponse

func main() {
	// Configuration flags
	var tcpPort = flag.Int("p", 10000, "Port to listen on")
	var appSecret = flag.String("s", "", "Slack app client secret")
	var appID = flag.String("i", "", "Slack app ID")
	flag.Parse()

	// Check that we have the required parameters
	if *appSecret == "" || *appID == "" {
		log.Fatal("Missing the Slack app secret and/or ID")
	}

	log.Printf("Starting the slack onboarding app. Listening on port %d", *tcpPort)

	// Add our / endpoint
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		code := r.URL.Query()["code"]
		if len(code) <= 0 {
			w.WriteHeader(400)
		} else {
			log.Printf("Received request with code %s", code[0])
		}

		// We have a code let's call oauth.v2.access
		res, err := http.Get("https://slack.com/api/oauth.v2.access?client_secret=" + *appSecret + "&client_id=" + *appID + "&code=" + code[0])
		if err != nil {
			log.Fatal(err)
		}

		// Read and parse the JSON body
		data, _ := ioutil.ReadAll(res.Body)
		res.Body.Close()
		log.Printf("%s\n", data)
		json.Unmarshal([]byte(data), &slackResponse)

		// Display the token to the user if we got one
		if slackResponse.Ok {
			fmt.Fprintf(w, "Your device token is: %s", slackResponse.AuthedUser.AccessToken)
		} else {
			fmt.Println("Request failed, please try again...")
		}
	})

	// We don't have a favicon
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
	})

	// Start our web server
	if err := http.ListenAndServe(fmt.Sprintf(":%d", *tcpPort), nil); err != nil {
		log.Fatal(err)
	}
}
