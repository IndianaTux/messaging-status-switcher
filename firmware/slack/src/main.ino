#include <FS.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoSlack.h>
#include <ArduinoJson.h>

// D3 and D4 can not be used because if they are grounded
// at boot it prevente the ESP8266 to boot
// Ref: https://docs.wemos.cc/en/latest/d1/d1_mini.html
#define LED_PIN D3
#define ONBOARD_LED_PIN D4
#define FIRMWARE_RESET_PIN D1
#define STATUS_1_PIN  D2
#define STATUS_2_PIN  D0
#define STATUS_3_PIN  D5
#define STATUS_4_PIN  D6
#define STATUS_5_PIN  D7
#define STATUS_6_PIN  D8

// HTTPS client instalne
WiFiClientSecure client;

// Slack API Token
char slackToken[100];

// Flag for saving data
bool shouldSaveConfig = false;

// Last status sent to Slack
int lastStatus = 0;


// Callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void setup() {
  // Start the serial console for debug
  Serial.begin(115200);

  // Turn the onboard LED off
  pinMode(ONBOARD_LED_PIN, OUTPUT);
  digitalWrite(ONBOARD_LED_PIN, HIGH);  // Onboard LED is active low

  // Turn the LED off
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  // Initialise out input pins
  pinMode(FIRMWARE_RESET_PIN, INPUT);
  pinMode(STATUS_1_PIN, INPUT);
  pinMode(STATUS_2_PIN, INPUT);
  pinMode(STATUS_3_PIN, INPUT);
  pinMode(STATUS_4_PIN, INPUT);
  pinMode(STATUS_5_PIN, INPUT);
  pinMode(STATUS_6_PIN, INPUT);

  // WiFiManager instance
  WiFiManager wifiManager;

  // Firmware reset procedure to prevent accidental firmware reset
  //  1. Power up with FIRMWARE_RESET_PIN == HIGH
  //  2. LED will flash 10 times (750ms)
  //  3. If FIRMWARE_RESET_PIN still HIGH, abort firmware reset
  //  4. If FIRMWARE_RESET_PIN is LOW, flash LED 20 times at 250ms
  //  5. If FIRMWARE_RESET_PIN is still LOW abort firmware reset
  //  6. If FIRMWARE_RESET_PIN is high go for the firmware reset
  int counter = 0;
  bool ledState = LOW;
  if (digitalRead(FIRMWARE_RESET_PIN) == HIGH) {
    Serial.println("Firmware reset asked... Starting stage 1.");
    for(counter=0 ; counter < 10 ; counter ++) {
      digitalWrite(LED_PIN, !ledState);
      ledState = !ledState;
      delay(750);
    }
    if (digitalRead(FIRMWARE_RESET_PIN) == LOW) {
      Serial.println("Firmware reset asked... Starting stage 2.");
      for(counter=0 ; counter < 20 ; counter ++) {
        digitalWrite(LED_PIN, !ledState);
        ledState = !ledState;
        delay(250);
      }
      if (digitalRead(FIRMWARE_RESET_PIN) == HIGH) {
        Serial.println("Firmware reset granted.");
        wifiManager.resetSettings();
        SPIFFS.format();
        while(true) {
          // Blink the LED fast indicating the factory reset is done
          digitalWrite(LED_PIN, !ledState);
          ledState = !ledState;
          delay(125);
        }
      }
      else {
        Serial.println("Firmware reset aborted at stage 2.");
      }
    }
    else {
      Serial.println("Firmware reset aborted at stage 1.");
    }
  }

  // Read configuration from FS json
  if (SPIFFS.begin()) {
    Serial.println("Mounted file system");
    if (SPIFFS.exists("/config.json")) {
      Serial.println("Reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        size_t size = configFile.size();
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument json(2048);
        DeserializationError deserializeError = deserializeJson(json, buf.get());
        serializeJsonPretty(json, Serial);

        if (!deserializeError) {
          strcpy(slackToken, json["slackToken"]);
        } else {
          Serial.println("Failed to load json config");
        }
        configFile.close();
      }
    }
  } else {
    Serial.println("Failed to mount FS");
  }

  // Add the custom slackToken configuration parameter
  WiFiManagerParameter custom_slackToken("slackToken", "Slack Token", slackToken, 100);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.addParameter(&custom_slackToken);


  // Auto-connect to wifi
  if (!wifiManager.autoConnect("SlackSwitcher")) {
    delay(3000);
    ESP.reset();
    delay(5000);
  }

  // If we get here we are connected to the WiFi
  Serial.println("Connected to Wifi");

  // Read the slack Token
  strcpy(slackToken, custom_slackToken.getValue());

  // If needed save the JSON config
  if (shouldSaveConfig) {
    DynamicJsonDocument json(1024);
    json["slackToken"] = slackToken;
    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("Failed to open config file for writing");
    }
    serializeJsonPretty(json, Serial);
    serializeJson(json, configFile);
    configFile.close();
  }

  // Show our local IP
  Serial.println("local ip");
  Serial.println(WiFi.localIP());

  // Slack client instance
  ArduinoSlack slack(client, slackToken);
  client.setFingerprint(SLACK_FINGERPRINT);

  // Our loop to read the switch and update the profile
  while(1) {
    SlackProfile profile;
    if (digitalRead(FIRMWARE_RESET_PIN) == HIGH) {
      Serial.println("FIRMWARE_RESET_PIN");
    }
    if (digitalRead(STATUS_1_PIN) == HIGH) {
      if(lastStatus != 1) {
        Serial.println("STATUS_1_PIN");
        slack.setCustomStatus("Available", ":office:");
        lastStatus = 1;
      }
    }
    if (digitalRead(STATUS_2_PIN) == HIGH) {
      if(lastStatus != 2) {
        Serial.println("STATUS_2_PIN");
        slack.setCustomStatus("Phone/Meeting", ":spiral_calendar_pad: ");
        lastStatus = 2;
      }
    }
    if (digitalRead(STATUS_3_PIN) == HIGH) {
      if(lastStatus != 3) {
        Serial.println("STATUS_3_PIN");
        slack.setCustomStatus("DND - Busy", ":no_entry:");
        lastStatus = 3;
      }
    }
    if (digitalRead(STATUS_4_PIN) == HIGH) {
      if(lastStatus != 4) {
        Serial.println("STATUS_4_PIN");
        slack.setCustomStatus("Available", ":house:");
        lastStatus = 4;
      }
    }
    if (digitalRead(STATUS_5_PIN) == HIGH) {
      if(lastStatus != 5) {
        Serial.println("STATUS_5_PIN");
        slack.setCustomStatus("On the road", ":car:");
        lastStatus = 5;
      }
    }
    if (digitalRead(STATUS_6_PIN) == HIGH) {
      if(lastStatus != 6) {
        Serial.println("STATUS_6_PIN");
        slack.setCustomStatus("Out of office", ":beach_with_umbrella:");
        lastStatus = 6;
      }
    }

    delay(1000);
  }

}

void loop() {
}

