# messaging-status-switcher

This mechanical status switch box project was designed to be used with Slack but with the right firmware can be used for other messaging platform as well.

It was inspired by the one made by Becky Stern and published on Instructable here: https://www.instructables.com/Slack-Status-Updater-With-ESP8266/

# Sponshoship

The developement of this project was sponsored by Intello Technologies.

Intello Technologies was founded by experts from hotel & IT industry backgrounds in 2003. Based in Montreal, Canada, Intello is more than just a services company or a simple VAR: they develop their own in-house software which they integrate with existing industry-leading hardware and then support for years to come.

Intello team has in-depth knowledge of software development combined with years of sales and field management experience in the hospitality and lodging industries. Their solutions have been deployed throughout the United States, Canada, Mexico, the Caribbean and even in Africa, and their helpdesk ensures their customers are supported around the clock. Recognized as industry leaders in our fields of expertise, their company goal is to provide great solutions and support.

Find out more on their web site at https://www.intello.com

# Electrical design

## Bill of material

|            Part            | Quantity | Possible source |
| -------------------------- | -------- | --------------- |
| Protoboard                 | 1        | https://www.universal-solder.ca/product/5-pcs-50x70-mm-prototyping-perfboard/                |
| Rotary switch              | 1        |  https://www.mouser.ca/ProductDetail/474-COM-13253               |
| Rotary switch knob         | 1        |  https://www.mouser.ca/ProductDetail/474-COM-09998               |
| Rotary switch breakout PCB | 1        | https://www.mouser.ca/ProductDetail/474-BOB-13098|
| D1-Mini ESP8266 controller | 1        | https://www.universal-solder.ca/product/esp8266-wifi-nodemcu-arduino-wemos-d1-mini-compatible/                |
| 10k resistor               | 7        |  https://www.mouser.ca/ProductDetail/Ohmite/ON1145E-R58?qs=HovGnFcXeQAJAxq8AtrkcQ%3D%3D               |

You will need some wires (wirewrap wire works great), soldering iron and some solder

## Schematic and wiring diagram

This device schematic is fairly simple and simply connects the rotary switch to the proper I/O on the ESP8266 compatible board. Pull-down resistor are added so that the switch can be properly read by the microcontroller.

## Wiring diagram

![alt text](doc/wiring_diagram.png)

## Schematic

![alt text](doc/schematic.png)

## Assembled electronics

![alt text](doc/PB260067.JPG)

![alt text](doc/PB260068.JPG)

![alt text](doc/PB260071.JPG)

# Enclosure

The enclosure is a simple design that can be 3D printed without support.  The design file (Sketchup) as well as STL files are available in the `enclosure` folder.

![alt text](doc/enclosure1.png)

![alt text](doc/enclosure2.png)

![alt text](doc/enclosure3.png)

![alt text](doc/PB260073.JPG)

# Slack

## Integration setup

Integration with slack is not the easiest. You will need a server acessible by the user(s) that will use the device where you will be able to run a small API (provided in the `onboarding` folder). This is needed as users will need to visit a special URL at Slack which will redirect to the server running the onboarding API so that the user gets it's personal OAuth token. This is called the "OAuth dance".

But first you need to create your Slack app. Here are the steps:

- Visit https://api.slack.com/apps and click on "Create New App"
- Enter a name for your app and select the Slack Workspace the app will be used in and click "Create App".
- On the left panel click on "OAuth & Permissions"
- Click on "Add New Redirect URL" and enter the URL to reach the onboarding API (this is the small piece of software you will need to install somewhere on a public server)
- Click on "Save URLs"
- In the "Scopes" - "User Token Scopes" section add the following Oauth scopes:
  - `users.profile:write`
  - `users:write`
- In the "OAuth Tokens & Redirect URLs" click "Install to Workspace"
- Click on "Basic Information" and take in note the following from the "App Credentials" section. You will need them for the onboarding URL and API:
  - "Client ID"
  - "Client Secret"
- Optionally you can add a description and logo for your app.

Then you need to compile and start the onboarding application on the public server of your choice. Building it is as simple as running `script/build_onboarding`. The onboarding script is writel in Go so you do have to instal Go if you don't already have it.

Once compiled copy the binary to the server you selected and start the process with:

```
screen ./slack -i CLIENT_ID -s CLIENT_SECRET
```

Where `CLIENT_ID` and `CLIENT_SECRET` are the information you took when setting up the app at Slack. By default this application answers to TCP port 10000. You can change this using the `-p` switch.

You are now ready to configuring your device.

## Configuring the device

First you need to obtain your personal OAuth token. To get this visit the following site:

https://slack.com/oauth/v2/authorize?client_id=CLIENT_ID&scope=&user_scope=users.profile:write,users:write

Where `CLIENT_ID` is again the one you got when setting up the app at Slack.

When acessing this URL you will be prompt to allow the app some permissions and after clicking "Allow" you will be redirected to the URL you assigned while setting up the app at Slack and you should get a page with a single line that looks like this:

```
Your device token is: xoxp-428000017012-430119846452-1469890676273-6e39415c7c58c5afcbabcd1c93202e8f
```

Copy this token in your clipboard as you will need it a bit later.

Now is the time to connect the device, if it was never configured it will broadcast a wifi network (SSID) called `SlackSwitcher`. When you see it go through these easy steps:

- Connect to this network and you should get a pop-up window to configure your wifi.
- From that window, click on the "Configure Wifi" button.
- Click on the wifi network you want the device to be attached to.
- Enter the wifi password in the next field
- Enter the OAuth token you got in the last field
- And finally click on "Save"

At this point the device should reboot and be able to talk to Slack to change your status according to the position of the rotary switch.

You can customize the status and emoji in the firmware code.

## Doing a factory reset

If for some reason you change your wifi you will need to factory reset the device and go through the above configuration process again.

A factory reset is initiated by a small procedure. Follow these simple 3 steps:

- Disconnect the device
- Rotate the switch to the leftmost position (2 clicks lower that the first status)
- Connect the device, the LED will start blinking
- While it's blinking change the rotary switch 1 porition to the right (you must do this within 30s). The LED should start blinking faster
- Change the rotary switch to again the left most position. The LED will turn off.
- Wait for the LED to light up solid. This indicates that the factory reset is completed
- Disconnect the device, wait 15s
- Connect the device

You should now see the `SlackSwitcher` wifi network and you will be able to setup your device again.

# Firmware

The firmware can be compiled with PlatformIO by simply running:

```
pushd firmware/slack
platformio run -t upload
popd
```

The above command will compile the Arduino code and upload it to the attached D1-Mini board.
